(function () {
  'use strict';

  angular
    .module('products')
    .run(menuConfig);

  menuConfig.$inject = ['Menus'];

  function menuConfig(menuService) {
    // Set top bar menu items
 

     menuService.addMenuItem('topbar', {
      title: 'List Products',
      state: 'products.list', 
      roles: ['*'],
    }); 
  }
}());
