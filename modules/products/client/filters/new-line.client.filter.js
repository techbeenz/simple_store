(function () {
  'use strict';

  angular
    .module('products')
    .filter('newLine', newLine);

  newLine.$inject = [/*Example: '$state', '$window' */];

  function newLine(/*Example: $state, $window */) {
    return function (input) {
      return input.replace(/(?:\r\n|\r|\n)/g, '<br/>');
      //return input?input.replace(/\n/g, '<br/>'):'';
    };
  }

 


})();
