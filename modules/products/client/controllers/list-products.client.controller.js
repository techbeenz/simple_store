(function () {
  'use strict';

  angular
    .module('products')
    .controller('ProductsListController', ProductsListController)
    .directive('resize', ChangeSize);



  ProductsListController.$inject = ['$scope','$location','ProductsService','Authentication'];

  function ProductsListController($scope,$location,ProductsService,Authentication) {
    var vm = this;

    vm.products = ProductsService.query();
    vm.username = Authentication.user.username;
 
	 
  }
 
  function ChangeSize($window)
  {
    return function (scope, element) {
       var w = angular.element($window); 
       var changeHeight = function() {
        var elements = $window.document.getElementsByClassName("panel-body");
        for (var i=0;i<elements.length; i++) {
            elements[i].style.height =  elements[i].offsetWidth+ 'px'; 
        }
      } 
      w.bind('resize', function () {        
              changeHeight();             
        }); 
       changeHeight();  
      } 

  }


}());
